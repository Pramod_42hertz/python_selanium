from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import unittest
import json
import logging
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 


class LoginTest(unittest.TestCase):
    testData = json.load(open('AddingActivity_Production.json'))
    Email_address = json.load(open('email.json'))
    @classmethod
    # initilizes the required data
    def setUpClass(inst):
        inst.driver = webdriver.Chrome()
        inst.driver.implicitly_wait(10)
        inst.driver.get("https://mytaptrack.auth.us-west-2.amazoncognito.com/login?response_type=token&client_id=61f7lakgd2mchf12dko1l0mabs&redirect_uri=https://portal.mytaptrack.com/dashboard&state=STATE&scope=openid+profile+aws.cognito.signin.user.admin+email")
        time.sleep(2)
        # inst.driver.find_element_by_class_name("style-jm3xvrt7label").click()
        cssSelectorOfSameElements = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(3) > input" 
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElements).send_keys("parent@mytaptrack.com")
        cssSelectorOfSameElementsForPwd = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(5) > input"
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElementsForPwd).send_keys("Inspire123")
        # time.sleep(2)
        inst.driver.find_element_by_css_selector("#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > input.btn.btn-primary.submitButton-customizable").click()#send_keys("keys.ENTER") 
        time.sleep(2)
    
    def setUp(self):
        time.sleep(7)
        self.driver.get("https://portal.mytaptrack.com/dashboard")


    def test_1_Add_Activity(self):
        select = self.testData['Add_Activity']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('Add_Activity_with_wrong_timeDiff: Faileds\n')
            f.close()
       
    
    def test_2_Add_Activity_kelly(self):
        select = self.testData['Add_Activity_kelly']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_2_Add_Activity_kelly: Faileds\n')
            f.close()
       
    
    def test_2_Add_Activity_with_empty_starttime(self):
        select = self.testData['Add_Activity_with_empty_starttime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_2_Add_Activity_with_empty_starttime: Faileds\n')
            f.close()
       
    
    def test_3_Add_Activity_with_empty_endtime(self):
        select = self.testData['Add_Activity_with_empty_endtime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_3_Add_Activity_with_empty_endtime: Faileds\n')
            f.close()
       
    
    def test_4_Add_Activity_with_empty_title(self):
        select = self.testData['Add_Activity_with_empty_title']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_4_Add_Activity_with_empty_title: Faileds\n')
            f.close()
       
    
    def test_5_Add_Activity_woth_empty_comments(self):
        select = self.testData['Add_Activity_with_empty_comments']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_5_Add_Activity_woth_empty_comments: Faileds\n')
            f.close()
       
    
    def test_6_Add_Activity_second_time(self):
        select = self.testData['Add_Activity_second_time']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_6_Add_Activity_second_time: Faileds\n')
            f.close()
       
    
    def test_7_Add_Activity_invalid_timeformat(self):
        select = self.testData['Add_Activity_invalid_timeformat']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_7_Add_Activity_invalid_timeformat: Faileds\n')
            f.close()
       
    
    def test_8_Add_Activity_with_wrong_timeDiff(self):
        global failed_functions
        select = self.testData['Add_Activity_with_wrong_timeDiff']
        time.sleep(2)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_8_Add_Activity_with_wrong_timeDiff: Faileds\n')
            f.close()
       
            
    
    def test_9_Add_Activity_with_same_time(self):
        select = self.testData['Add_Activity_with_same_time']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_9_Add_Activity_with_same_time: Faileds\n')
            f.close()
    
    def test_10_Add_Activity_valid_second_time(self):
        select = self.testData['Add_Activity_valid_second_time']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
                print "function error"
                f= open ('text12.txt','a+')
                f.write('test_10_Add_Activity_valid_second_time: Faileds\n')
                f.close()
    
    def test_11_Add_2_Activity(self):
        select = self.testData['Add_2_Activity']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
                print "function error"
                f= open ('text12.txt','a+')
                f.write('test_11_Add_2_Activity: Faileds\n')
                f.close()
    
    def test_12_deleting_Activity(self):
            select = self.testData['deleting_Activity']
            time.sleep(2)
            self.preform(select)
            error = self.preform(select)
            if error == False:
                print "function error"
                f= open ('text12.txt','a+')
                f.write('test_12_deleting_Activity: Faileds\n')
                f.close()
       


    def test_13_editing_Activity_with_unfilled_starttime(self):
        select = self.testData['editing_Activity_with_unfilled_starttime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_13_editing_Activity_with_unfilled_starttime: Faileds\n')
            f.close()
       
        

    def test_14_editing_Activity_with_unfilled_endtime(self):
        select = self.testData['editing_Activity_with_unfilled_endtime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_14_editing_Activity_with_unfilled_endtime: Faileds\n')
            f.close()
       

    def test_15_editing_Activity_with_unfilled_title(self):
        select = self.testData['editing_Activity_with_unfilled_title']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_15_editing_Activity_with_unfilled_title: Faileds\n')
            f.close()
       

    def test_16_editing_Activity_with_invalidToValid_starttime(self):
        select = self.testData['editing_Activity_with_invalidToValid_starttime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_16_editing_Activity_with_invalidToValid_starttime: Faileds\n')
            f.close()
       

    def test_17_editing_Activity_with_invalidToValid_endtime(self):
        select = self.testData['editing_Activity_with_invalidToValid_endtime']
        time.sleep(2)
        self.preform(select)
        error = self.preform(select)
        if error == False:
            print "function error"
            f= open ('text12.txt','a+')
            f.write('test_17_editing_Activity_with_invalidToValid_endtime: Faileds\n')
            f.close()
       

    def test_18_sendemail(self):
        select = self.Email_address['email']
        fromaddr = ""
        passw = ""
        toaddr = ""
        for i in select:
            if i['action'] == 'from':
                fromaddr = i['path']
            elif i['action'] == 'password':
                passw = i['path']
            else:
                toaddr = i['path']
        
        print fromaddr
        # instance of MIMEMultipart 
        msg = MIMEMultipart() 
        
        # storing the senders email address   
        msg['From'] = fromaddr 
        
        # storing the receivers email address  
        msg['To'] = toaddr 
        print msg['To']
        # storing the subject  
        msg['Subject'] = "Test Automation Log "
        
        # string to store the body of the mail 
        body = "Body_of_the_mail"
        
        # attach the body with the msg instance 
        msg.attach(MIMEText(body, 'plain')) 
        
        # open the file to be sent  
        filename = "text12.txt"
        attachment = open("C:/python_selanium/Production website/text12.txt", "r") 
        
        # instance of MIMEBase and named as p 
        p = MIMEBase('application', 'octet-stream') 
        
        # To change the payload into encoded form 
        p.set_payload((attachment).read()) 
        
        # encode into base64 
        encoders.encode_base64(p) 
        
        p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
        
        # attach the instance 'p' to instance 'msg' 
        msg.attach(p) 
        
        # creates SMTP session 
        s = smtplib.SMTP('smtp.office365.com', 587) 
        s.ehlo()
        # start TLS for security 
        s.starttls() 
        
        s.ehlo()
        
        # Authentication 
        s.login(fromaddr, passw) 
        
        # Converts the Multipart msg into a string 
        text = msg.as_string() 
        
        # sending the mail 
        s.sendmail(fromaddr, toaddr, text) 
        
        # terminating the session 
        s.quit()



    



            
    @classmethod
    def tearDownClass(inst):
        time.sleep(3)
        inst.driver.close()
    
    def preform(self,select):
        # time.sleep(1)
        return_error = True
        for i in select:
            # print i
            if i['action'] != "-click-":
               self.driver.find_element_by_css_selector(i['path']).clear()

               try: 
                if self.driver.find_element_by_css_selector(i['path']):
                    self.driver.find_element_by_css_selector(i['path']).send_keys(i['action'])
               except:
                   return_error= False
                   print "error in send key"
                   return return_error
                # time.sleep(0.5)
            else:
               time.sleep(2)
               try:
                if self.driver.find_element_by_xpath(i['path']):
                   
                    self.driver.find_element_by_xpath(i['path']).click()
                        
                   
               except:
                    return_error= False
                    print "error in xpath"
                    return return_error
                    
                    
                
            # time.sleep(2)
        time.sleep(0.5)



# logging.basicConfig(filename=log_file, level=logging.DEBUG, extra=failed_functions)
if __name__ == "__main__":
    # its a main method calls setupclass method
    unittest.main(verbosity=1)






