from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import unittest
import json
import logging
class glyphiconTest(unittest.TestCase):
    testData = json.load(open('glyphicon.json'))

    @classmethod
    # initilizes the required data
    def setUpClass(inst):
        inst.driver = webdriver.Chrome()#Firefox()#
        inst.driver.implicitly_wait(10)
        inst.driver.get("https://mytaptrack.auth.us-west-2.amazoncognito.com/login?response_type=token&client_id=61f7lakgd2mchf12dko1l0mabs&redirect_uri=https://portal.mytaptrack.com/dashboard&state=STATE&scope=openid+profile+aws.cognito.signin.user.admin+email")
        time.sleep(2)
        # inst.driver.find_element_by_class_name("nav-link").click()
        cssSelectorOfSameElements = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(3) > input" 
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElements).send_keys("parent@mytaptrack.com")
        cssSelectorOfSameElementsForPwd = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(5) > input"
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElementsForPwd).send_keys("Inspire123")
        # time.sleep(4)
        inst.driver.find_element_by_css_selector("#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > input.btn.btn-primary.submitButton-customizable").click()#send_keys("keys.ENTER") 
    # 
    def setUp(self):
        time.sleep(6)
        self.driver.get("https://portal.mytaptrack.com/dashboard")


    def test_41_fb(self):
        select = self.testData['Test41_fb']
        time.sleep(2)
        self.preform(select)

    def test_42_instagram(self):
        select = self.testData['Test42_instagram']
        time.sleep(2)
        self.preform(select)
    
    def test_43_twitter(self):
        select = self.testData['Test43_twitter']
        time.sleep(2)
        self.preform(select)
    
    def test_44_linkedin(self):
        select = self.testData['Test44_linkedin']
        time.sleep(2)
        self.preform(select)
    
    def test_45_printerst(self):
        select = self.testData['Test45_printerest']
        time.sleep(2)
        self.preform(select)
    
    def test_46_terms(self):
        select = self.testData['Test46_terms']
        time.sleep(2)
        self.preform(select)
    
    def test_47_privacy(self):
        select = self.testData['Test47_privacy']
        time.sleep(2)
        self.preform(select)

    def test_48_AWS(self):
        select = self.testData['AWS']
        time.sleep(2)
        self.preform(select)

    def test_49_Logicworks(self):
        select = self.testData['Logicworks']
        time.sleep(2)
        self.preform(select)

    @classmethod
    def tearDownClass(inst):
        time.sleep(6)
        inst.driver.close()
    

    def preform(self, select):
        # time.sleep(1)
        for i in select:
            if i['action'] != "-click-":
                self.driver.find_element_by_css_selector(i['path']).clear()
                self.driver.find_element_by_css_selector(
                    i['path']).send_keys(i['action'])
                # time.sleep(0.5)
            else:
                self.driver.find_element_by_xpath(i['path']).click()
                # time.sleep(0.5)
        time.sleep(4)


logging.basicConfig(filename="glyph.log", format='%(asctime)s: %(levelname)s: %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)
if __name__ == "__main__":
    # its a main method calls setupclass method
    unittest.main(verbosity=1)
