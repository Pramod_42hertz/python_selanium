from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import unittest
import json
import logging
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 


import os
from os.path import join,dirname
from dotenv import load_dotenv

dotenv_path = os.path.normpath(join(os.getcwd(), '.env'))
load_dotenv(dotenv_path)


class LoginTest(unittest.TestCase):
    testData = json.load(open('Edit_student.json'))
    @classmethod
    # initilizes the required data
    def setUpClass(inst):
        inst.driver = webdriver.Chrome()#Edge()#
        inst.driver.implicitly_wait(10)
        inst.driver.get("http://localhost/")
        time.sleep(2)
        inst.driver.find_element_by_class_name("nav-link").click()
        cssSelectorOfSameElements = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(3) > input" 
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElements).send_keys("parent@mytaptrack.com")
        cssSelectorOfSameElementsForPwd = "#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(5) > input"
        inst.driver.find_element_by_css_selector(cssSelectorOfSameElementsForPwd).send_keys("Inspire123")
        time.sleep(4)
        inst.driver.find_element_by_css_selector("#div-forms > div:nth-child(2) > div:nth-child(2) > div > div > form > input.btn.btn-primary.submitButton-customizable").click()#send_keys("keys.ENTER") 
    
    # def setUp(self):
    #     time.sleep(6)
    #     self.driver.get("http://localhost/dashboard")


    def test_1_edit_student_kelly(self):
        select = self.testData['edit_student_kelly']
        time.sleep(4)
        self.preform(select)
    
    def test_1_edit_student_kelly_empty_fname(self):
        select = self.testData['edit_student_kelly_empty_fname']
        time.sleep(4)
        self.preform(select)

    def test_1_edit_student_kelly_empty_lname(self):
        select = self.testData['edit_student_kelly_empty_lname']
        time.sleep(4)
        self.preform(select)

    def test_1_edit_student_kelly_empty_district(self):
        select = self.testData['edit_student_kelly_empty_district']
        time.sleep(4)
        self.preform(select)
   
            
    @classmethod
    def tearDownClass(inst):
        time.sleep(4)
        inst.driver.close()
    
    def preform(self,select):
        # time.sleep(1)
        for i in select:
            if i['action'] != "-click-":
                self.driver.find_element_by_css_selector(i['path']).clear()
                self.driver.find_element_by_css_selector(
                    i['path']).send_keys(i['action'])
                time.sleep(0.5)
            else:
                if i['type'] == 'xpath':
                    self.driver.find_element_by_xpath(i['path']).click()
                    time.sleep(1)
                elif i['type'] == 'css':
                    self.driver.find_element_by_css_selector(
                    i['path']).click()
                else:
                    self.driver.find_element_by_link_text(i['path']).click()

            time.sleep(2)
        time.sleep(2)



logging.basicConfig(filename="test_behavior1.log", format='%(asctime)s: %(levelname)s: %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)
if __name__ == "__main__":
    # its a main method calls setupclass method
    unittest.main(verbosity=2)
    





